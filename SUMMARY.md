- [Deploy vs. Create Pod](Markdown/02-Deploy-Vs-Create-Pod.md)
- [Visualização de Logs](Markdown/03-Logs.md)
- [Limitação de Recursos](Markdown/04-Limitacao-de-recursos.md)
- [Interação com o Pod](Markdown/05-Interacao-Pod.md)
- [Criação de Imagens](Markdown/06-Criacao-de-Imagens.md)
- [Utilização de Imagem Criada](Markdown/07-Utilizando-imagem-criada.md)
- [Labels e Services](Markdown/08-Create-Labels-Service.md)
  - [Tipos de Services](Markdown/08.1-Tipos-de-Service.md)
- [Ingress](Markdown/09-Ingress-NGINX.md)
  - [Tipos de Ingress](Markdown/09.1-Tipos-de-Ingress.md)
- [Volumes](Markdown/10-Volumes.md)
  - [Volume Persistente Local](Markdown/10.1-Persistent-Volume-Local.md)
- [Secrets](Markdown/11-Secrets.md)

<!-- TOC -->
- 📚 Refrências
  - [Documentação Official (EN)](https://kubernetes.io/docs/concepts/)